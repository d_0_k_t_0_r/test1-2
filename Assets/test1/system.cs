﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class system : MonoBehaviour
{
    private int cliki=0;
    private Text text;

    private void Start()
    {
        text = this.gameObject.GetComponent<Text>();
    }

    public void clik() => theGame();

    void theGame()
    {
        cliki++;
        view();
    }

    void view()
    {
        text.text = "Все клики: "+ cliki.ToString();
    }
}
