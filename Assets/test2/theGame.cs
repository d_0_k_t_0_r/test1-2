﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class theGame : MonoBehaviour
{
    public GameObject prefub;
    private bool dataOK;
    private float time,t,s,d;

    private void Start()
    {
        time = 0;
        dataOK = false;
    }

    private void Update()
    {
        if (dataOK == true && time <= 0)
        {
            GameObject pr = Instantiate(prefub, new Vector3(0, 0, 0), Quaternion.identity);
            pr.GetComponent<cube>().info(s, d);
            time = t;
        }
        else time -= Time.deltaTime;
    }

    public void create(float timer,float speed,float distance)
    {
        t = timer;
        s = speed;
        d = distance;
        dataOK = true;
    }
}
