﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cube : MonoBehaviour
{
    private float s, d;

    void Update()
    {
        transform.position += transform.forward * s * Time.deltaTime;
        if (Vector3.Distance(Vector3.zero, transform.position) >= d) Destroy(this.gameObject);
    }

    public void info(float speed, float distance)
    {
        s = speed;
        d = distance;
    }
}
