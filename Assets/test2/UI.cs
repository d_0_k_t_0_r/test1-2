﻿using UnityEngine;
using UnityEngine.UI;

public class UI : MonoBehaviour
{
    public Text time, speed, distance;
    public theGame theGame;
    
    public void onStart()
    {
        if(time!=null && speed!=null & distance != null)
        {
            theGame.create(float.Parse(time.text),float.Parse(speed.text),float.Parse(distance.text));
        }
    }
}
